V 14
1
LANG:1 0 
PANEL,-1 -1 666 123 N "_3DFace" 1
"$2"
"main() {
  string label;
  if(isDollarDefined(\"$2\")) { label = $2; }
  
  string partition = ESSExtension_convertLabelToPartition(label);  
  setValue(\"BAR\", \"text\", BAR.text + partition); // Set header title
  
  __populateTable(partition); // Init & callback for updating table.
}

" 0
 E E E E 1 -1 -1 0  0 0
""0  1
E "#uses \"CMS_ECALfw_ESSExtension/ESSExtension_sensorsManipulation.ctl\"

// colors
const string BAD_COLOR      = \"FwStateAttention3\";
const string GOOD_COLOR     = \"FwStateOKPhysics\";
const string DISABLED_COLOR = \"FwDead\";

// TABLE columns
const string COL_SENSOR  = \"SENSOR\";
const string COL_VALUE   = \"VALUE\";
const string COL_OVER    = \"OVERHEAT\";
const string COL_WIRE    = \"WIRE_BROKEN\";

// other constants & declarations
const string FLOAT_FORMAT = \"%.2f\";

/**
  It creates row in the TABLE for every sensor in the partition. 
*/
public void __populateTable(string partition) {
  smpos position = ESSExtension_getPosition(partition);
  if(!ESSExtension_isBarrel(position)) { // Only EE has water leak sensors connected.
    string sensorDp = ESSExtension_getWaterLeakageSensor(partition);
    dpConnect(\"__CB_SensorStatusChanged\", sensorDp + \"value.converted\", sensorDp + \"qualityFlags.channelWireBroken\");
  }
}

/**
  Callback to initializate and update lines in the table
  @note: this function will refresh table rows on the following events: Sensor value change, Sensor wire is broken.
*/
private void __CB_SensorStatusChanged(const string &sensorValueDp, const float &sensorValue, const string &sensorWireDpe, const bool &wireBroken) {
  string partition   = ESSExtension_getPartitionFromDP(sensorValueDp);
  int    sensorIndex = 1; // there is only 1 waterleaksensor per partition (if any).
  
  string sensorName = dpSubStr(sensorValueDp, DPSUB_DP) + \".\";
    
  float Threshold;
  dpGet(sensorName + \"upperLimit.converted\", Threshold);
    
  string  colour_val = \"white\", colour_limit = \"white\", colour_sensor = \"white\";
  if(sensorValue >= Threshold)     { colour_limit  = \"FwStateAttention3\"; colour_val = \"FwStateAttention3\"; }
  
  if(!ESSExtension_isSensorEnabled(sensorName)) { // grey out
    colour_val    = DISABLED_COLOR; 
    colour_sensor = DISABLED_COLOR;
    colour_limit  = DISABLED_COLOR; 
  }
  
  dyn_string S_Sensor     = makeDynString(sensorName                , colour_sensor);
  dyn_string S_Value      = makeDynString(__formatFloat(sensorValue), colour_val);
  dyn_string S_Leakage    = makeDynString(__formatFloat(Threshold)  , colour_limit);
  dyn_string S_WireBroken = (wireBroken ? makeDynString(\"broken\", \"FwStateAttention3\") : makeDynString(\"Ok\", colour_sensor));
  
  TABLE.updateLine(1, COL_SENSOR , S_Sensor , COL_VALUE , S_Value     ,
                      COL_OVER   , S_Leakage, COL_WIRE  , S_WireBroken );
  delay(0, 50); // Previous operation needs time to finish before setting tooltip
  
  /* Now set tooltips */
  int cellRow  = sensorIndex -1; // table counts from zero
  string alias = ESSExtension_getAlias(sensorName);
  string tooltip = sensorName + \"\\n-------------------------------------------------------------\\n\" + alias;
  TABLE.cellToolTipRC(cellRow, COL_SENSOR, tooltip);
}

/**
  Converts float value to string using a 2 decimal format
*/
private string __formatFloat(float value) {
  string stringValueConverted;
  sprintf(stringValueConverted, FLOAT_FORMAT, value);
  return stringValueConverted;
}

" 0
 3
"CBRef" "1"
"EClose" E
"dpi" "96"
0 0 0
""
DISPLAY_LAYER, 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0
LAYER, 0 
1
LANG:1 0 
6 2
"Border2"
""
1 0.06298377571567493 0 E E E 1 E 1 E N "_WindowText" E N {135,135,202} E E
 E E
1 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E -1 1 1 0 1 E 0.9124388920208755 0 0.6333333333333333 -0.05746884652930175 0 1 E 0 0 696 30
2 3
"BAR"
"BAR"
1 11.04875079821204 10 E E E 1 E 1 E N "white" E N "_Transparent" E E
 E E
2 0 0 0 0 0
E E E
0
1
LANG:1 0 

3
"layoutAlignment" "AlignCenter"
"dashclr"N "_Transparent"
"antiAliased" "0"
E E 0 1 1 0 1 E U  0 E 5.048750798212041 3 332.048750798212 19
0 2 0 "0s" 0 0 0 64 0 0  5.048750798212041 3 1
1
LANG:1 26 Arial,-1,13,5,50,0,0,0,0,0
0 1
LANG:1 26 Water leakage sensor for: 
25 4
"TABLE"
"TABLE"
1 5 32 E E E 1 E 1 E N "_WindowText" E N "white" E E
 E E
3 0 0 0 0 0
E E E
0
1
LANG:1 0 

1
"layoutAlignment" "AlignNone"
1
LANG:1 35 MS Shell Dlg 2,-1,11,5,50,0,0,0,0,0
0  -2 20 637 87
EE 1 0 1 4 0 "SENSOR" 42 1 0 "s" 1
LANG:1 6 Sensor
E
1
LANG:1 0 

330 "WIRE_BROKEN" 6 1 0 "[0s,,,ALIGNMENT_CENTER]" 1
LANG:1 4 Wire
E
1
LANG:1 0 

60 "VALUE" 5 1 0 "[0s,,,ALIGNMENT_CENTER]" 1
LANG:1 5 Value
E
1
LANG:1 0 

60 "OVERHEAT" 5 1 0 "[0s,,,ALIGNMENT_CENTER]" 1
LANG:1 5 Limit
E
1
LANG:1 0 

60 
20 20 10 0
1
LANG:1 35 MS Shell Dlg 2,-1,11,5,50,0,0,0,0,0
0 0 1 1 1 7
1 0
0
LAYER, 1 
1
LANG:1 0 
0
LAYER, 2 
1
LANG:1 0 
0
LAYER, 3 
1
LANG:1 0 
0
LAYER, 4 
1
LANG:1 0 
0
LAYER, 5 
1
LANG:1 0 
0
LAYER, 6 
1
LANG:1 0 
0
LAYER, 7 
1
LANG:1 0 
0
0
