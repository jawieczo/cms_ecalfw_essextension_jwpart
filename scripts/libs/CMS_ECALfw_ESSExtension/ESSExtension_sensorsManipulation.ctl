/**
 *  @file    ESSExtension_sensorsManipulation.ctl 
 *  @author  Raul Jimenez Estupinan, ETHZ IPP (CERN EP/UCM), <Raul.Jimenez.Estupinan@cern.ch>
 *           Irena Veljanovic      , CERN EP/UCM           , <Irena.Veljanovic@cern.ch>   
 *  @version 1.0.0
 *  @date Last modification:    16/04/2019
 *  - Library with the sensors manipulation functions for the ESS Extension.
 *  - This library uses some of the features of the CTRL++, such as anum types. 
 * 
 *  ## Dependencies: fwConfigs, CMS_ECALfw_Utils
 */

#uses "fwConfigs/fwAlertConfig"
#uses "CMS_ECALfw_Utils/ECALfw_Utils.ctl"

global string g_CMS_ECALfw_ESS_SYSTEM_NAME = "";
const string CMS_ECALfw_ESSExtension = "CMS_ECALfw_ESSExtension";

// Main paterns encoded in the DP aliases
const string ESSroot          = "ESS/";
const string ESSDSS           = "DSS/";
const string ESSOverheating   = "Overheating/";
const string ESSRelays        = "Relays/";
const string ESSGeneralRelays = "General/";
const string ESSWaterleak     = "Waterleak/";

// Type of relays per SM
const string COOL  = "COOL";
const string HV    = "HV";
const string LV    = "LV";
const string LVCut = "LVCut";

// Type of general relays
const string ESS_PLC_Watchdog = "ESS_PLC_Watchdog";
const string ESS_PLC_Alive    = "ESS_PLC_Alive";
const string COOL_PLC_Watchdog    = "COOL_PLC_Watchdog";
const string COOL_PLC_Alive = "COOL_PLC_Alive";
const string DSS_CRITICAL_Overheating  = "DSS_CRITICAL_Overheating";

// Type of Sensor according to TK TOOL
const string READ     = "Read";
const string WRITE    = "Write";
const string READBACK = "Readback";

// PLC parameters needed for the handshake
const int     __PLCID         = 598;
const string  __PLCCON        = "ESS";
const int     __WRITE_DELAY   = 10; // ms
const int     __ACK_SCANCYCLE = 650; // must be more than 100 ms in production to be processed in the same scan cycle.
    
// Constant threshold to trigger FSM automatic action: Threshold = PLC.ConvertedLowerLimit - FsmAA (5 degrees).
const int  __FSM_AA_Threshold = 5; // Celsius degrees to sustract from PLC threshold. 

const mapping lvRackMapping = __ESSExtension_initializeLvRackMapping();



/**
    Enumeration for ECAL positions, including quadrants 
    Helper functions to manipulate the ECAL positions (Enum). 
*/
enum smpos {
  EBP01 =  1, EBP02 =  2, EBP03 =  3, EBP04 =  4, EBP05 =  5, EBP06 =  6, EBP07 =  7, EBP08 =  8, EBP09 =  9, 
  EBP10 = 10, EBP11 = 11, EBP12 = 12, EBP13 = 13, EBP14 = 14, EBP15 = 15, EBP16 = 16, EBP17 = 17, EBP18 = 18,
  EBM01 = 19, EBM02 = 20, EBM03 = 21, EBM04 = 22, EBM05 = 23, EBM06 = 24, EBM07 = 25, EBM08 = 26, EBM09 = 27,
  EBM10 = 28, EBM11 = 29, EBM12 = 30, EBM13 = 31, EBM14 = 32, EBM15 = 33, EBM16 = 34, EBM17 = 35, EBM18 = 36,
  EEP01 = 37, EEP02 = 38, EEP03 = 39, EEP04 = 40, EEM01 = 41, EEM02 = 42, EEM03 = 43, EEM04 = 44
};

/**
  Generates an array of with the ECAL positions using the enumerated type.
 Since enumKeys feature is not yet present, it builts the array manually until WinCCOA3.16 or Patch13.
  The array should match the SM pos 1..44 and order is ensured by enumKeys.
    @return supermodule positions.
  */
public dyn_string ESSExtension_getPartitions() {
  // dyn_string partitions = enumKeys("smpos"); // available in P013
  dyn_string partitions;
  partitions = makeDynString("EBP01", "EBP02", "EBP03", "EBP04", "EBP05", "EBP06", "EBP07", "EBP08", "EBP09",
                             "EBP10", "EBP11", "EBP12", "EBP13", "EBP14", "EBP15", "EBP16", "EBP17", "EBP18",
                             "EBM01", "EBM02", "EBM03", "EBM04", "EBM05", "EBM06", "EBM07", "EBM08", "EBM09",
                             "EBM10", "EBM11", "EBM12", "EBM13", "EBM14", "EBM15", "EBM16", "EBM17", "EBM18",
                             "EEP01", "EEP02", "EEP03", "EEP04", "EEM01", "EEM02", "EEM03", "EEM04");
  return partitions;
}


/**
  Returns the partition names as they should be displayed in the FSM.
  It can be used to create or manipulate the fsm.
  It compress the Dee names using the Far/near terminology.
  @note in PCConf to configure the FSM
  @see CMS_ECALfw_ESSExtension_InstallConfig
*/
public dyn_string ESSExtension_getLabels() {
  dyn_string labels = makeDynString();
  dyn_string partitions    = ESSExtension_getPartitions();
  for(int index = 1; index <= dynlen(partitions); index++) {
    dynAppend(labels, ESSExtension_convertPartitionToLabel(partitions[index]));
  }
  dynUnique(labels);

  return labels;
}

public string ESSExtension_convertLabelToPartition(string partitionLabel) {
  string partitionName = partitionLabel;

  strreplace(partitionName, "_ESS", ""); // remove _ESS suffix
  if(strpos(partitionName, "EE") == 0) { // If EE convert the F/N terms.
    strreplace(partitionName, "EEP_F", "EEP01");
    strreplace(partitionName, "EEP_N", "EEP03");
    strreplace(partitionName, "EEM_N", "EEM01");
    strreplace(partitionName, "EEM_F", "EEM03");
  }

  strreplace(partitionName, "P_", "P"); // remove hyphen
  strreplace(partitionName, "M_", "M"); // remove hyphen

  return partitionName;
}

/**
  This function converts the partition name into the FSM name using the Full Dee terminology
  for the endcaps (meaning return up to 40 different labels, unless the expand option is used). 
  This function must remain as it is, since it's being used in other components such as OPFCAutomaticActions.
*/
public string ESSExtension_convertPartitionToLabel(string partitionName, bool expanded = FALSE) {
  string partitionLabel = partitionName;
  smpos position        = ESSExtension_getPosition(partitionName);
  if(!expanded && !ESSExtension_isBarrel(position)){
    string compressedName = ESSExtension_resolvePartitionName(partitionName); // compress Dee names.
    position              = ESSExtension_getPosition(compressedName);
    
    if(position == smpos::EEP01) partitionLabel = "EEP_F_ESS";
    if(position == smpos::EEP03) partitionLabel = "EEP_N_ESS";
    if(position == smpos::EEM01) partitionLabel = "EEM_N_ESS";
    if(position == smpos::EEM03) partitionLabel = "EEM_F_ESS";
  } else {
    // simply add underscores and suffix
    strreplace(partitionLabel, "P", "P_");
    strreplace(partitionLabel, "M", "M_");
    partitionLabel += "_ESS";
  }

  return partitionLabel;
}


//Note that comparators <= or >= need explicit conversion to Integer.
public bool ESSExtension_isBarrel(smpos position) {
  bool isBarrel = FALSE;
  if((int) position <= (int) smpos::EBM18) isBarrel = TRUE;  
  return isBarrel;
}

//Note that comparators <= or >= need explicit conversion to Integer.
public bool ESSExtension_isPlus(smpos position) {
  bool isPlus = FALSE;
  if((int) position <= (int) smpos::EBP18) isPlus = TRUE;
  if((int) position == (int) smpos::EEP01) isPlus = TRUE;
  if((int) position == (int) smpos::EEP02) isPlus = TRUE;
  if((int) position == (int) smpos::EEP03) isPlus = TRUE;
  if((int) position == (int) smpos::EEP04) isPlus = TRUE;
  return isPlus;
}

public bool ESSExtension_isMinus(smpos position) {
  return !ESSExtension_isPlus(position);
}

public string ESSExtension_nextPartition(string partition) {
  int position = (int)ESSExtension_getPosition(partition) + 1;
  smpos nextPartition = (smpos) position;
  return ESSExtension_getPartitionNameFromEnum(nextPartition);
}


/**
  Translate a numeric position into an ECAL partition name.
    @param  position   // Supermodule position, EE/EB positions including quadrants. 
    @return            // Supermodule name
  */
public string ESSExtension_getPartitionNameFromPosition(int position) {
  dyn_string partitions = ESSExtension_getPartitions();
  
  return partitions[position]; /* @todo: need out-of-bound check. */
}
/*
public string ESSExtension_getPartitionNameFromPosition(int position) {
  mapping sm_map = enumValues("smpos");
  for (int i = 1; i <= mappinglen(sm_map); i++) {
    if(mappingGetValue(sm_map, i) == position) return mappingGetKey(sm_map, i);
  }
  return "Unknown";
}
*/


/**
  Translate an enumerated position into an ECAL partition name.
    @param  position Supermodule position declared in the enum smpos.
    @return // Supermodule name
  */
public string ESSExtension_getPartitionNameFromEnum(smpos position) {
  //dyn_string partitions = enumKeys("smpos");
  dyn_string partitions = ESSExtension_getPartitions();
  return partitions[(int) position];
}


/**
  Translate an ECAL partition name into an enumerated position declared in smpos
    @param  partitionName
    @return  smpos  // partition position declared in the enum smpos.
  */
public smpos ESSExtension_getPosition(string partitionName) {
  dyn_string partitions = ESSExtension_getPartitions();
  int position = dynContains(partitions, partitionName);
  return (smpos) position;
}
/*
public smpos ESSExtension_getPosition(string partitionName) {
  mapping sm_map = enumValues("smpos");
  if(!mappingHasKey(sm_map, partitionName)) {
    DebugTN("[ERROR] ESSExtension_getPosition unable to find the key: " + partitionName);
  }
  return (smpos) sm_map[partitionName];
}
*/


/* ***********************************************************************************************************
    Core functionality of the ESS Extension component
*********************************************************************************************************** */

/**
  Resolve the datapoint name for a particular sensor defined by partition name and sensor number.
  It distinguishes between the different copies of the sensor: overheating and critical. 
  It also permits to retrieve the name for the different type copies: Read/write/read-back
    @param  partitionName    // dp alias (eg. EBM01)
    @param  sensor           // sensor position in the supermodule (1..8).
    @param  sensorType       // Copy of the sensor: ESSOverheating or ESSDSS (critical overheating copy).
    @param  TKDPType         // Type of copy: READ/WRITE/READBACK
    @return                  // datapoint name
  */

public string ESSExtension_getSensorDp(string partitionName, int sensor = 0, string sensorType = ESSOverheating, string TKDPType = READ) {
  string completeAlias = ESSroot + sensorType + partitionName + "/" + TKDPType + "/Sensor0" + sensor;
  string dp = dpAliasToName(completeAlias);
  
  if(TKDPType == WRITE)    strreplace(dp, "_read_", "_write_");
  if(TKDPType == READBACK) strreplace(dp, "_read_", "_readback_");
  
  if((dp == "") || !dpExists(dp)) {
    ECALfw_throw("ESSExtension_getSensorDp: Unable to resolve sensor dp for "+  partitionName, ERROR, CMS_ECALfw_ESSExtension);
  }
  
  return dp;
}

/**
  Resolve the datapoint name for a waterleakage sensor using the given partition name.
  NOTE: 
    * Water leakage sensors are only enabled/monitored for the EE. 
    * There are only 4 WLK sensors for both endcaps, but wiring and objects in the PLC are for 8 sensors.
    * One out two sensors mapped for each quadrant are fake.
    * Alias format as: ESS/Waterleak/EEM04/Read/Sensor01
  @param  partitionName    // (eg. EEM01)
  */
public string ESSExtension_getWaterLeakageSensor(string partitionName) {
  string completeAlias = ESSroot + ESSWaterleak + partitionName + "/" + READ + "/Sensor01";
  string dp = dpAliasToName(completeAlias);
  
  if((dp == "") || !dpExists(dp)) ECALfw_throw("Unable to resolve waterleak sensor dp for: " + completeAlias , WARNING, CMS_ECALfw_ESSExtension);
  
  return dp;
}

/**
  Resolve the datapoint names for all the sensors sensor defined by partition name.
  It distinguishes between the different copies of the sensor: overheating and critical. 
  It also permits to retrieve the name for the different type copies: Read/write/read-back
    @param  partitionName    // dp alias (eg. EBM01)
    @param  sensorType       // Copy of the sensor: ESSOverheating or ESSDSS (critical overheating copy).
    @param  TKDPType         // Type of copy: READ/WRITE/READBACK
    @return                  // datapoint name
  */
public dyn_string ESSExtension_getSensorDps(string partitionName, string sensorType = ESSOverheating, string TKDPType = READ) {
  dyn_string sensorDps = makeDynString();
  for(int index = 1; index <= 8; index++) {
    string dp = ESSExtension_getSensorDp(partitionName, index, sensorType, TKDPType);
    if((dp == "") || !dpExists(dp)) ECALfw_throw("ESSExtension_getSensorDps: Unable to resolve sensor dps: "+ partitionName, ERROR, CMS_ECALfw_ESSExtension);
    else dynAppend(sensorDps, dp);
  }
  return sensorDps;
}

/**
 Retrieves all enabled temperature sensors of a certain type/mode
    @note Enable/disable can only be determined at the READ version of the sensor.
    There is no .dcs.updated element in the write/readback version.
*/
public dyn_string ESSExtension_getAllEnabledSensors(string sensorType = ESSOverheating) {
  dyn_string datapoints;
  
  dyn_string partitions = ESSExtension_getPartitions();  
  for(int position = 1; position <= dynlen(partitions); position++) {
    string partition    = partitions[position];
    dyn_string sensorDps = ESSExtension_getEnabledSensorDps(partition, sensorType);
    dynAppend(datapoints, sensorDps);
  }

  return datapoints;
}

/**
  Retrieves all LV relays (read version?)
  @brief to list alarms for notifying a manual interlock
  @see __ESSExtension_attachAlarmsToNotifications
*/
public dyn_string ESSExtension_getAllRelays(string relayDPType = READ, string relayType = LV) {
  dyn_string datapoints;
  dyn_string partitions = ESSExtension_getPartitions();  
  for(int position = 1; position <= dynlen(partitions); position++) {
    string partition    = partitions[position];
    string relay = ESSExtension_getRelay(partition, relayDPType, relayType);
    if(relay != "" && dpExists(relay)) dynAppend(datapoints, relay);
  }
  return datapoints;
}

/**
  Similar to ESSExtension_getSensorDps, but only returns dps for non disabled sensors.
    @param  partitionName    // dp alias (eg. EBM01)
    @param  sensorType       // Copy of the sensor: ESSOverheating or ESSDSS (critical overheating copy).
    @return                  // datapoint name
    
    @note Enable/disable can only be determined at the READ version of the sensor.
    There is no .dcs.updated element in the write/readback version.
*/
public dyn_string ESSExtension_getEnabledSensorDps(string partitionName, string sensorType = ESSOverheating) {
  bool isEnabled;
  dyn_string enabledSensorDps = makeDynString();
  
  dyn_string sensorDps = ESSExtension_getSensorDps(partitionName, sensorType, READ);
  for(int index = 1; index <= dynlen(sensorDps); index++) {
    if(ESSExtension_isSensorEnabled(sensorDps[index])) { dynAppend(enabledSensorDps, sensorDps[index]); }
  }
  
  return enabledSensorDps;
}

/**
  Return whether a sensor is enabled/disabled according to Tracker definitions.
  @param sensorDp // sensor datapoint with DOT at the end
*/
public bool ESSExtension_isSensorEnabled(string sensorDp) {
  bool isEnabled = TRUE;
  bool notUpdated, goodness, upperEnable, lowerEnable, monitoringEnable;
  
  dpGet(sensorDp + "dcs.notUpdated", notUpdated);
  dpGet(sensorDp + "qualityFlags.goodness", goodness);
  dpGet(sensorDp + "qualityFlags.upperLimitEnable", upperEnable);
  dpGet(sensorDp + "qualityFlags.lowerLimitEnable", lowerEnable);
  dpGet(sensorDp + "qualityFlags.enableMonitoring", monitoringEnable);
  
  if(!goodness && !upperEnable && !lowerEnable && !monitoringEnable) {
    isEnabled = FALSE;
  }

  return isEnabled;
}

/**
  Return whether a sensor is updated according to tracker definitions.
  for this function to work, it is needed to run the tracker script with watch dog.
*/
public bool ESSExtension_isSensorUpdated(string sensorDp) {
  bool notUpdated = TRUE;
  dpGet(sensorDp + "dcs.notUpdated", notUpdated);
  
  return !notUpdated;
}

/**
  Resolve the datapoint name for a particular digital input using the alias.
    @param  alias      // dp alias (eg. ESS/DI/Cooling/EBP01/Valve)
    @return  datapoint // EMPTY if not existing. 
  */
public string ESSExtension_getDigitalInputDp(string alias) {
  string datapoint = dpNames(dpAliasToName(alias), "TkPlcReadDigitalInput");
  if((datapoint == "") || !dpExists(datapoint)) ECALfw_throw("ESSExtension_getSensorDp: Unable to resolve DI dp: "+ alias, ERROR, CMS_ECALfw_ESSExtension);
  
  return datapoint;
}

public dyn_string ESSExtension_getAllDigitalInputs() {
  dyn_string dpes, aliases;
  string aliasFilter, dpeFilter;
  aliasFilter = "ESS/*";
  dpeFilter = "*channel_read_di*.";
  dpGetAllAliases(dpes, aliases, aliasFilter, dpeFilter);
  
  return dpes;
}

/**
  Resolve the datapoint name for digital inputs modeling partition cooling volves.
    @param  partition  // (eg. EBP01)
    @return  datapoint // EMPTY if not existing. 
  */
public string ESSExtension_getCoolingValve(string partition) {
  dyn_string dpes, aliases;
  string aliasFilter, dpeFilter;
  string datapoint = "";
  
  partition = ESSExtension_resolvePartitionName(partition); // Needed for the EE
  aliasFilter = "ESS/*" + partition + "*Valve";
  dpeFilter = "*channel_read_di*.";
  dpGetAllAliases(dpes, aliases, aliasFilter, dpeFilter);
  
  if(dynlen(dpes) != 1) {
    ECALfw_throw("ESSExtension_getCoolingValve: Unable to resolve digital input datapoint for " + partition, ERROR, CMS_ECALfw_ESSExtension);
    ECALfw_throw(aliases, ERROR, CMS_ECALfw_ESSExtension);
    ECALfw_throw(dpes, ERROR, CMS_ECALfw_ESSExtension);
  } else datapoint = dpes[1];
  
  return datapoint;
}

/**
  Get all the Digital input datapoints for the ECAL cooling valves.
  No special filter is applied.
    @return dyn_string  datapoints
  */
public dyn_string ESSExtension_getListCoolingValve() {
  dyn_string dpes, aliases;
  string aliasFilter, dpeFilter;
  string datapoint = "";
  
  aliasFilter = "ESS/*Valve";
  dpeFilter = "*channel_read_di*.";
  dpGetAllAliases(dpes, aliases, aliasFilter, dpeFilter);
  
  if(dynlen(dpes) == 0) DebugTN("[ERROR] ESSExtension_getListCoolingValve: Unable to resolve digital input datapoints");
  
  return dpes;
}

/**
  Get all the Digital input datapoints for the ECAL LV rack leak sensors.
  No special filter is applied.
    @return dyn_string  datapoints
  */
public dyn_string ESSExtension_getListLvRackFlow() {
  dyn_string dpes, aliases;
  string aliasFilter, dpeFilter;
  string datapoint = "";
  
  aliasFilter = "ESS/DI/LVRack/Flow/*";
  dpeFilter = "*channel_read_di*.";
  dpGetAllAliases(dpes, aliases, aliasFilter, dpeFilter);
  
  if(dynlen(dpes) == 0) DebugTN("[ERROR] ESSExtension_getListCoolingValve: Unable to resolve digital input datapoints");
  
  return dpes;
}

/**
  Get all the Digital input datapoints for the ECAL LV rack leak sensors.
  No special filter is applied.
    @return dyn_string  datapoints
  */
public dyn_string ESSExtension_getListLvRackLeak() {
  dyn_string dpes, aliases;
  string aliasFilter, dpeFilter;
  string datapoint = "";
  
  aliasFilter = "ESS/DI/LVRack/Leak/*";
  dpeFilter = "*channel_read_di*.";
  dpGetAllAliases(dpes, aliases, aliasFilter, dpeFilter);
  
  if(dynlen(dpes) == 0) DebugTN("[ERROR] ESSExtension_getListCoolingValve: Unable to resolve digital input datapoints");
  
  return dpes;
}

public string ESSExtension_getLvRackLeakDp(string partition) {
  //partition = ESSExtension_resolvePartitionName(partition); // Needed for the EE
  string lvRack = lvRackMapping[ESSExtension_getPosition(partition)];
  string alias = "ESS/DI/LVRack/Leak/"+lvRack;
  
  return ESSExtension_getDigitalInputDp(alias);
}

public string ESSExtension_getLvRackFlowDp(string partition) {
  //partition = ESSExtension_resolvePartitionName(partition); // Needed for the EE
  string lvRack = lvRackMapping[ESSExtension_getPosition(partition)];
  string alias = "ESS/DI/LVRack/Flow/"+lvRack;
  
  return ESSExtension_getDigitalInputDp(alias);
}


/* @todo ESSfw_getPlcSystemName */
public string ESSfw_getPlcSystemName() {
  // ToDo: Use the function to resolve system name using component names from DB.
  return ""; // return empty (current system) just for debugging.
}

/**
  Resolves/compress quadrant names into the first partition of the quadrant.
  @note: Accepts barrel names.
  The main purpose is to translate into the main partition name for Dees:
  EEPF: EEP01 --> EEP01, EEP02 --> EEP01
  EEPN: EEP03 --> EEP03, EEP04 --> EEP03
  EEMF: EEM01 --> EEM01, EEM02 --> EEM01
  EEMN: EEM03 --> EEM03, EEM04 --> EEM03
    
  EB: returns the same value (no translation). 
*/
public string ESSExtension_resolvePartitionName(string partition) {
  smpos partitionPos = ESSExtension_getPosition(partition);
  
  if(partitionPos == smpos::EEP02) partitionPos = smpos::EEP01;
  if(partitionPos == smpos::EEP04) partitionPos = smpos::EEP03;
  if(partitionPos == smpos::EEM02) partitionPos = smpos::EEM01;
  if(partitionPos == smpos::EEM04) partitionPos = smpos::EEM03;
  
  return ESSExtension_getPartitionNameFromEnum(partitionPos);
}


public int ESSExtension_getNumberOfSensors() {
  dyn_string sensors = dpNames("TK_PLCS/" + __PLCCON + "*_read_*", "TkPlcReadSensor"); // 708: 704 temp + 4 WLD
  return dynlen(sensors);
}

/**
  Acknowledges a problem (interlock) for a particular partition.
  In the Tracker tool, the acknowledgement happens at the sensor level for all the copies: ESSDSS / ESSOverheating.
  NOTE: The acknowledge procedure needs to target all sensors (with or without TRUE bit for acknowlegment. 
    @param  partition partitionName    // (eg. EBM01)
    @return bool                       // acknowled success
  */
public bool ESSExtension_acknowledgePartition(string partition) {
  dyn_string sensorsToAcknowledge = makeDynString();
  for(int i = 1; i <= 8; i++) {
    string sensorDpDSS  = ESSExtension_getSensorDp(partition, i, ESSDSS);         // sensorDp comes with dot already
    string sensorDpOver = ESSExtension_getSensorDp(partition, i, ESSOverheating); // sensorDp comes with dot already

    dynAppend(sensorsToAcknowledge, sensorDpDSS);
    dynAppend(sensorsToAcknowledge, sensorDpOver);
  }
  
  if(!ESSExtension_isBarrel(ESSExtension_getPosition(partition))) {
    dynAppend(sensorsToAcknowledge, ESSExtension_getWaterLeakageSensor(partition));
  }
    
  DebugTN(partition, sensorsToAcknowledge);
  int NumberOfEntries = ESSExtension_getNumberOfSensors(); // all entries, not only the selection.
  
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.writingStatus", 1);
  time t = getCurrentTime();
  string timestamp = formatTime("%y:%m:%d:%H:%M:%S",t, "");
  dyn_string str = strsplit(timestamp,":");
  dyn_int getBCD;
  for (int a=1; a<=dynlen(str); a++)
  {
    getBCD[a] = ((int)str[a]/10)*16 + (int)str[a]%10;
  }

  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.year", getBCD[1]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.month", getBCD[2]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.day", getBCD[3]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.hour", getBCD[4]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.minute", getBCD[5]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp1.second", getBCD[6]+0);
  
  // WE DO ACKNOWLEDGE
  for(int sensorIndex = 1; sensorIndex <= dynlen(sensorsToAcknowledge); sensorIndex++) {
    dpSetWait(dpSubStr(sensorsToAcknowledge[sensorIndex], DPSUB_DP) + ".acknowledge", TRUE);
    delay(0, __WRITE_DELAY); //ms
    
  }
  
  time t1 = getCurrentTime();
  string timestamp1 = formatTime("%y:%m:%d:%H:%M:%S",t1, "");
  dyn_string str1 = strsplit(timestamp1,":");
  dyn_int getBCD1;
  for (int a=1; a<=dynlen(str1); a++)
  {
    getBCD1[a] = ((int)str1[a]/10)*16 + (int)str1[a]%10;
  }
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.year", getBCD1[1]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.month", getBCD1[2]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.day", getBCD1[3]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.hour", getBCD1[4]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.minute", getBCD1[5]);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.timestamp2.second", getBCD1[6]+1);
  
  //dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.crc", 192);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.plcId", __PLCID);
  dpSet(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.numberOfEntries", NumberOfEntries);
  dpSetWait(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "/Configurations/ackConf.writingStatus", 2);
  
  // UNK acknowledge after the handshake (no handshake needed for this)
      
  delay(0, __ACK_SCANCYCLE); // must be more than 100 ms to be processed in the same scan cycle.
  __ESSExtension_unacknowledgePartition(partition);
  return TRUE;
}


/**
  The unacknowledge is part of the ACKnowledge procedure, prevents from leaving acknowledges (TRUE)
  by writing zeros (unacknowledge) to all the sensors after acknowleging.
    @param  partition partitionName    // (eg. EBM01)
    @return                            // UNacknowled success
  */
private bool __ESSExtension_unacknowledgePartition(string partition) {
  dyn_string sensorsToUnacknowledge = makeDynString();
  for(int i = 1; i <= 8; i++) {
    string sensorDpDSS  = ESSExtension_getSensorDp(partition, i, ESSDSS);         // sensorDp comes with dot already
    string sensorDpOver = ESSExtension_getSensorDp(partition, i, ESSOverheating); // sensorDp comes with dot already
    dynAppend(sensorsToUnacknowledge, sensorDpDSS);
    dynAppend(sensorsToUnacknowledge, sensorDpOver);
  }
  
  // No need to trigger handshake
  
  // WE DO ACKNOWLEDGE
  for(int sensorIndex = 1; sensorIndex <= dynlen(sensorsToUnacknowledge); sensorIndex++) {
    string dpe = dpSubStr(sensorsToUnacknowledge[sensorIndex], DPSUB_DP) + ".acknowledge";
    bool acknowledged = FALSE;
    dpGet(dpe, acknowledged);
    if(acknowledged) {
      dpSetWait(dpe, FALSE); // set to FALSE only if acknowledged
      delay(0, __WRITE_DELAY); //ms
    }
  }
  
  // No need to close handshake
  return TRUE;
}



/**
 UNAcknowledge procedure to put zeros everywhere. 
 We don't need to trigger the handshake for this, just blank the memory before acknowledging. 
    @return                     // UNacknowled success
  */
public bool ESSExtension_unacknowledgeAllInterlocks() {  

  DebugTN("ESSExtension_unacknowledgeAllInterlocks", "Unacknowlesing ALL sensors...");
  dyn_string DPs = dpNames(ESSfw_getPlcSystemName() + "TK_PLCS/" + __PLCCON + "*","TkPlcReadSensor");
  for (int a=1; a<=dynlen(DPs); a++) {
    dpSetWait(dpSubStr(DPs[a], DPSUB_SYS_DP) + ".acknowledge", FALSE);
    delay(0, __WRITE_DELAY); //ms
  }
  DebugTN("ESSExtension_unacknowledgeAllInterlocks", "DONE");
}


/**
  Return an array of relay datapoints for a given partition
    @param  partition partition name    // (eg. EBM01) // alias encoded into the dp
    @param  RelayDPType      // READ, WRITE, READBACK
    @return dyn_string               // array of relay datapoints
  */
public dyn_string ESSExtension_getRelays(string partition, string RelayDPType = READ) {
  dyn_string relays = makeDynString();
  string dp;
  
  dp = ESSExtension_getRelay(partition, RelayDPType, COOL);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getRelay(partition, RelayDPType, HV);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getRelay(partition, RelayDPType, LV);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getRelay(partition, RelayDPType, LVCut);
  if(dp != "") dynAppend(relays, dp);
  
  return relays;
}

/**
  return a relay datapoint for the relay datapoint for a given supermodule and relay type
    @param  partition partition name    // (eg. EBM01)
    @param  RelayDPType      // READ, WRITE, READBACK
    @param  RelayType        // COOL, LV, LVCut, HV
    @return                   // relay datapoint
  */
public string ESSExtension_getRelay(string partition, string RelayDPType = READ, string RelayType = COOL) {
  dyn_string relays = makeDynString();
  string ALIASPrefix = ESSroot + ESSRelays + partition + "/" + RelayDPType + "/";
  string RelayAlias  = ALIASPrefix + RelayType;
  string dp = dpAliasToName(RelayAlias);
  
  return dp;
}

/**
  return and array of general DSS relays for a given RelayDPType
    @param  RelayDPType      // READ, WRITE, READBACK
    @return dyn_string       // array of relay datapoints
  */
public dyn_string ESSExtension_getGeneralRelays(string RelayDPType = READ) {
  dyn_string relays = makeDynString();
  string dp;
  
  dp = ESSExtension_getGeneralRelay(partition, RelayDPType, ESS_PLC_Watchdog);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getGeneralRelay(partition, RelayDPType, COOL_PLC_Watchdog);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getGeneralRelay(partition, RelayDPType, ESS_PLC_Alive);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getGeneralRelay(partition, RelayDPType, COOL_PLC_Alive);
  if(dp != "") dynAppend(relays, dp);
  dp = ESSExtension_getGeneralRelay(partition, RelayDPType, DSS_CRITICAL_Overheating);
  if(dp != "") dynAppend(relays, dp);  
  
  return relays;
}

/**
  return general DSS relay for a given RelayDPType and RelayType
    @param  RelayDPType      // READ, WRITE, READBACK
    @param  RelayType        // ESS_PLC_Watchdog, COOL_PLC_Watchdog, ESS_PLC_Alive, COOL_PLC_Alive, DSS_CRITICAL_Overheating
    @return                  // relay datapoint
  */
public string ESSExtension_getGeneralRelay(string RelayDPType = READ, string RelayType = DSS_CRITICAL_Overheating) {
  dyn_string relays = makeDynString();
  string ALIASPrefix = ESSroot + ESSRelays + ESSGeneralRelays + RelayDPType + "/";
  string RelayAlias  = ALIASPrefix + RelayType;
  string dp = dpAliasToName(RelayAlias);
  
  return dp;
}

/**
  Extract the partition name from the datapoint alias
  NOTE: The alias can be formed by 4 or 5 elements.
    @param  datapoint    // a TK datapoint for Sensor or Relay object
    @return               // Partition name
  */
public string ESSExtension_getPartitionFromDP(string datapoint) {
  string dp = dpSubStr(datapoint, DPSUB_DP); // trim element name from the dp.
  string alias = dpGetAlias(dp + "."); // resolve to alias
  dyn_string path = strsplit(alias, "/"); // expected 4 or 5 items. (eg. ESS/Relays/EBP01/Read/COOL).
  if(dynlen(path) > 3) return path[3]; // Return SM name.
  
  return "";
}

/**
  Extract the sensor number from the datapoint alias
  NOTE: The alias can be formed by 4 or 5 elements.
    @param  datapoint    // a TK datapoint for Sensor
    @return               // Partition name
  */
public int ESSExtension_getSensorIndexFromDP(string datapoint) {
  string sensor = "";
  string dp = dpSubStr(datapoint, DPSUB_DP); // trim element name from the dp.
  string alias = dpGetAlias(dp + "."); // resolve to alias
  dyn_string path = strsplit(alias, "/"); // expected 5 items. (eg. ESS/Overheating/EEP01/Read/Sensor08).
  if(dynlen(path) > 3) sensor = path[5]; // Sensor part of the alias
  strreplace(sensor, "Sensor", ""); // remove string
  int sensorIndex = sensor;
  
  return sensorIndex;
}


/**
  Return the alias of a Sensor or Relay datapoint.
  NOTE: The alias can be formed by 4 or 5 elements.
    @param  datapoint    // a TK datapoint for Sensor or Relay object
    @return               // Partition name
  */
public string ESSExtension_getAlias(string datapoint) {
  string dp = dpSubStr(datapoint, DPSUB_DP); // trim element name from the dp.
  return dpGetAlias(dp + "."); // resolve to alias
}


/**
  This function exposes a datapoint generated by the tracker tool.
  Express the heartbeat of the PLC by reading a timer.
*/
public string ESSExtension_getPlcHeartBeatDp() {
  string systemName = "";
  return systemName + "TK_PLCS/" + __PLCCON + "/Configurations/readtimestamp.timestamp1.second";
  return systemName;
}

/**
  This datapoint tell us the S7 connection state.
    1: GOOD
  !=1: unknown
  @note: not using the CMS_TRACKER_GENERAL/tkHeartbeat.ctl
*/
public string ESSExtension_getPlcConnectionStatusDp() {
  string systemName = "";
  return systemName + "_PLC_" + __PLCCON + ".ConnState";
}

/**
  This function tell us a datapoint that updates constantly by the S7 driver if OK.
  @note: not using the CMS_TRACKER_GENERAL/tkHeartbeat.ctl
*/
public string ESSExtension_getPlcAliveStatusDp() {
  string systemName = "";
  return systemName + "_PLC_" + __PLCCON + ".State.RcvTelegrams";
}


/**
  Set the KILL value TRUE|FALSE for the relays of a given partition
  IMPORTANT: We DO NOT want to set COOL interlock.
  The cooling interlock is set by the Cooling PLC, and received by the ESS to operate.
    @param  SM partition    // partition name (eg. EBM01)
    @param  interlock    // TRUE: interlock, FALSE: remove interlock.
  */
public void ESSExtension_setManualInterlock(string SM, bool interlock = TRUE) {

    string relayDP_LV    = ESSExtension_getRelay(SM, WRITE, LV   );
    string relayDP_LVCut = ESSExtension_getRelay(SM, WRITE, LVCut);
    string relayDP_HV    = ESSExtension_getRelay(SM, WRITE, HV   );
    
    if(relayDP_LV   != "") dpSet(relayDP_LV    + KILL, interlock);
    if(relayDP_LVCut!= "") dpSet(relayDP_LVCut + KILL, interlock);
    if(relayDP_HV   != "") dpSet(relayDP_HV    + KILL, interlock);
}


/**
  Return the last part of a relay datapoint alias: COOL, LV, LVCut, HV
    @param  datapoint   // for a relay
    @return  relayType   // COOL, LV, LVCut, HV
  */
public string ESSExtension_getRelayType(string datapoint) {
  string dp = dpSubStr(datapoint, DPSUB_DP); // trim element name from the dp.
  string alias = dpGetAlias(dp + "."); // resolve to alias
  dyn_string path = strsplit(alias, "/"); // expected 4 or 5 items. (eg. ESS/EBP01/Relays/Read/COOL).
  if(dynlen(path) == 5) return path[5]; // Return SM name.
  
  return "";
}


/**
  Returns an alarm configuration object for a single analog alarm as upper limit.
    @param  warningLimit  // for a temperature sensor
    @param  fatalLimit    // for a temperature sensor
    @return dyn_mixed alertCOnfigObject // JCOP configuration object to assign the alarm limits. 
    
    @todo: this function is not yet used. Maybe can be removed.
  */
private dyn_mixed __ESSExtension_getAlarmObject_RangesUperLimit(float warningLimit, float fatalLimit) {
  dyn_mixed alertConfigObject;
  dyn_string alertTexts   = makeDynString("","Exceeded maximum warning threshold (" + warningLimit + ")", "Exceeded maximim error threshold (" + fatalLimit + ")");
  dyn_float alertLimits   = makeDynFloat(0, warningLimit, fatalLimit);
  dyn_string alertClasses = makeDynString("", "_fwErrorNack_70", "_fwFatalNack_90");
  string alertPanel = ""; // none
  dyn_string alertPanelParameters = makeDynString(); // none
  string alertHelp = ""; // none
  dyn_bool limitsIncluded = makeDynString(FALSE, FALSE, FALSE);
  dyn_string exceptionInfo;
  if(dynlen(exceptionInfo) > 0) {
    DebugN("[__ESSExtension_getAlarmObject_RangesUperLimit][ERROR]");
    DebugN(exceptionInfo);
  }
  fwAlertConfig_objectCreateAnalog(alertConfigObject, 
                              					alertTexts,
                              					alertLimits,
                              					alertClasses,
                              					alertPanel,
                              					alertPanelParameters,
                              					alertHelp,
                                   limitsIncluded,
                              					exceptionInfo);
  return alertConfigObject;
}

/**
  Returns whether a datapoint element has an alarm configured.
  @note that it uses the framework functions to determine existance.
  @param dpe datapoint element.
*/
public bool ESSExtension_hasAlarm(string dpe) {
  dyn_string exceptionInfo;
  dyn_mixed alertConfigObject = makeDynMixed();
  int configType;
  bool hasAlarm = FALSE;
  
  if(dpExists(dpe)) {
    fwAlertConfig_objectGet(dpe, alertConfigObject, exceptionInfo);
    if(dynlen(exceptionInfo)) DebugTN("ERROR at ESSExtension_hasAlarm");
    hasAlarm = fwAlertConfig_objectConfigExists(alertConfigObject, configType, exceptionInfo);
  }
  return hasAlarm;
}

/* ***********************************************************************************************************
    Initializations
*********************************************************************************************************** */

private const mapping __ESSExtension_initializeLvRackMapping() {
  mapping _lvRackMapping = makeMapping();
  
  _lvRackMapping[smpos::EBP01] = "X3J01";
  _lvRackMapping[smpos::EBP02] = "X3J01";
  _lvRackMapping[smpos::EBP03] = "X3J01";
  _lvRackMapping[smpos::EBP04] = "X3J01";
  _lvRackMapping[smpos::EBP05] = "X3J01";
  
  _lvRackMapping[smpos::EBP06] = "X3V01";
  _lvRackMapping[smpos::EBM03] = "X3V01";
  _lvRackMapping[smpos::EBM02] = "X3V01";
  _lvRackMapping[smpos::EBM01] = "X3V01";
  _lvRackMapping[smpos::EBM18] = "X3V01";
  
  _lvRackMapping[smpos::EBP07] = "X3A01";
  _lvRackMapping[smpos::EBP08] = "X3A01";
  _lvRackMapping[smpos::EBP09] = "X3A01";
  _lvRackMapping[smpos::EBP10] = "X3A01";
  _lvRackMapping[smpos::EBP11] = "X3A01";
  
  _lvRackMapping[smpos::EBM04] = "X3S01";
  _lvRackMapping[smpos::EBM05] = "X3S01";
  _lvRackMapping[smpos::EBM06] = "X3S01";
  _lvRackMapping[smpos::EBM07] = "X3S01";
  _lvRackMapping[smpos::EBM08] = "X3S01";
  
  _lvRackMapping[smpos::EBM17] = "X2U02";
  _lvRackMapping[smpos::EBM16] = "X2U02";
  
  _lvRackMapping[smpos::EBP18] = "X2U01";
  _lvRackMapping[smpos::EBP17] = "X2U01";
  
  _lvRackMapping[smpos::EBM10] = "X2R01";
  _lvRackMapping[smpos::EBM11] = "X2R01";
  
  _lvRackMapping[smpos::EBM09] = "X2R02";
  _lvRackMapping[smpos::EBP12] = "X2R02";
  
  _lvRackMapping[smpos::EBM13] = "X0U12";
  _lvRackMapping[smpos::EBM14] = "X0U12";
  _lvRackMapping[smpos::EBM15] = "X0U12";
  
  _lvRackMapping[smpos::EBM12] = "X0U11";
  _lvRackMapping[smpos::EBP16] = "X0U11";
  
  _lvRackMapping[smpos::EBP13] = "X0R11";
  _lvRackMapping[smpos::EBP14] = "X0R11";
  _lvRackMapping[smpos::EBP15] = "X0R11";
  
  _lvRackMapping[smpos::EEP01] = "X3A33";
  _lvRackMapping[smpos::EEP02] = "X3A33";
  _lvRackMapping[smpos::EEP03] = "X3J33";
  _lvRackMapping[smpos::EEP04] = "X3J33";
  _lvRackMapping[smpos::EEM01] = "X3V33";
  _lvRackMapping[smpos::EEM02] = "X3V33";
  _lvRackMapping[smpos::EEM03] = "X3S33";
  _lvRackMapping[smpos::EEM04] = "X3S33";
  
  return _lvRackMapping;
}

/** Retreives the name of the PVSS system where ESS component is installed
 * 
 * @return Returns the string with the name of the PVSS system where ESS component is installed
 */
synchronized string getESSSystemName()
{
  if(g_CMS_ECALfw_ESS_SYSTEM_NAME == "") {
    dyn_string list;
    fwInstallation_getApplicationSystem("CMS_ECALfw_ESSExtension", list);
    if(dynlen(list) == 0) {
      ECALfw_throw("Could not determine which system CMS_ECALfw_ESS is installed on. Returning local system name.", ERROR, CMS_ECALfw_ESSExtension);
      return getSystemName();
    }
    g_CMS_ECALfw_ESS_SYSTEM_NAME = list[1];
  }

  return g_CMS_ECALfw_ESS_SYSTEM_NAME;
}

// X4 PLC functionality
public void ESSExtension_powerCycleX4Racks() {
  bool isReset;
  do {
    dpSet(getESSSystemName() + "CMS_ECAL_DCS/ECALX4_Power_Reset.setting.relay", true);
    delay(0,500);
    dpGet(getESSSystemName() + "CMS_ECAL_DCS/ECALX4_Power_Reset.readback.relay", isReset);
    dpSet(getESSSystemName() + "CMS_ECAL_DCS/ECALX4_Power_Reset.setting.relay", false);
  } while(!isReset);
}
