#uses "CMS_ECALfw_ESSExtension/ESSExtension_sensorsManipulation.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

void main()
{
  string dataDirectory = "data/CMS_ECALfw_ESSExtension/data/";
  if(!isdir(dataDirectory)) {
    mkdir(dataDirectory, "777");  
  }
  
  string fileName = dataDirectory + "warning_" + formatTime("%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".csv";
  file csvFile = fopen(fileName, "w");
  string path = getPath(fileName);
  
  DebugTN("Csv file generation started. Csv file: " + path);  
  
  dyn_string aliases;
  dyn_string readSensors;
  
  dpGetAllAliases(readSensors, aliases, "ESS/Overheating/*/Read/Sensor*");
  

  string C; // celcius suffix
  sprintf(C, "[%cC]", 370);
  
  fprintf(csvFile, "%45s;%36s;%4s;%17s;%18s;%21s;%22s;%13s;%14s\r\n", "read overheating sensor dp", "read overheating sensor alias", "piw", "warning limit *C", "warning limit raw", "overheating limit *C", "overheating limit raw", "dss limit *C", "dss limit raw"); 
  for(int i = 1, limit = dynlen(readSensors); i <= limit; i++) {
   
    string row = ESSExtension_createLine(readSensors[i]);
    
    fprintf(csvFile, row);  
  }
  
  fclose(csvFile);

  DebugTN("Csv file generation finished. Csv file: " + path);  
}

/**
  Function creates string line: read overheating sensor dp; read overheating sensor alias; piw; warning limit *C; warning limit raw; overheating limit *C; overheating limit raw; dss limit *C; dss limit raw
*/
string ESSExtension_createLine(const string &aReadOverheatingSensor)
{
  string readAlias = dpGetAlias(aReadOverheatingSensor);
  int piw;
  dpGet(aReadOverheatingSensor + "piw.expected", piw); 
  
  // limits
  float warningLimitCelcius, overheatingLimitCelcius, dssLimitCelcius; // celcius
  int warningLimitRaw, overheatingLimitRaw, dssLimitRaw; // raw
    
  dpGet(aReadOverheatingSensor + "lowerLimit.converted", overheatingLimitCelcius); // overheating celcius
  dpGet(aReadOverheatingSensor + "lowerLimit.raw", overheatingLimitRaw); // overheating raw
  
  warningLimitCelcius = overheatingLimitCelcius - 2; // warning celcius
  
  string writeSensor = aReadOverheatingSensor; 
  strreplace(writeSensor, "read", "write");
  dpSetWait(writeSensor + "lowerLimit.converted", warningLimitCelcius); // dpFunc invoked
  dpGet(writeSensor + "lowerLimit.raw", warningLimitRaw); // warning raw
  
  string dssReadAlias = readAlias;
  strreplace(dssReadAlias, "Overheating", "DSS");   
  string readDssSensor = dpAliasToName(dssReadAlias);  
  
  dpGet(readDssSensor + "lowerLimit.converted", dssLimitCelcius); // dss celcius
  dpGet(readDssSensor + "lowerLimit.raw", dssLimitRaw); // dss raw
  
  string row;
  sprintf(row, "%45s;%36s;%4d;%17.3f;%18d;%21.3f;%22d;%13.3f;%14d\r\n", dpSubStr(aReadOverheatingSensor,DPSUB_DP), readAlias, piw, warningLimitCelcius, warningLimitRaw, overheatingLimitCelcius, overheatingLimitRaw, dssLimitCelcius, dssLimitRaw); 
  return row; 
}
