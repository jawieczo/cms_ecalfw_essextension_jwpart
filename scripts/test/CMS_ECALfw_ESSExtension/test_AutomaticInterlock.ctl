/**
  @file test_AutomaticInterlock.ctl
  @author  Jakub WIeczorek, CERN EP/CMX/DA, <jakub.lukasz.wieczorek@cern.ch>
  @version 1.0.0
  @date Last modification:    31/07/2019

  This script performs the interlocks test, that is it exceeds the thresholds for every temperature 
  (both overheating and critical overheating limit), wld
  sensor separatelly, checks if interlock was set, comes to previous temperature and finally acknowledge
  the partition in ordern to get rid off interlock. Before the test there is a process of clearing 
  environment - cleare fake VALUEWRITE sensors, set for them 700 (overheating) and 4500 (WLD) and 
  acknowledge every partition. After the test every fake sensor is deleted. 
 
  The test generates long report with summary table. Log file can be opened and tracked live.
  
 ## Dependencies: CMS_ECALfw_ESSExtension, fwUnitTestComponent
*/

#uses "CMS_ECALfw_ESSExtension/ESSExtension_sensorsManipulation.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

const int __MAX_ACKNOWLADGE_ATTEMPS = 10;
const int __EXPECTED_ACKNOWLADGE_ATTEMPTS = 1;
const int __ACKNOWLADGE_DELAY = 5;
const int __INTERLOCK_DELAY = 5;
const int __INTERLOCK_DELAY_WLD = 10;
const int __REWRITE_DELAY = 5;

const int __NORMAL_TEMPERATURE = 700;
const int __NORMAL_WLD = 4500;
const int __EXCEEDED_WLD = 3500;


/**
  In order to extend current test file:
  1) Add your test function in the main foor loop, which will return an array of faulty sensors #1
  2) Add new faulty array #2
  3) Append that array by new sensors set returned by your test function #3
  4) Check if the current array is empty
  5) Extend ESSExtension_performResultLog function by adding new array as a parameter and display their content following the patern in the function logic
*/
main()
{
  string dataDirectory = "data/CMS_ECALfw_ESSExtension/data/";
  if(!isdir(dataDirectory)) {
    mkdir(dataDirectory, "777");  
  }

  string fileName = "data/CMS_ECALfw_ESSExtension/data/interlock_test_" + formatTime("%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".log";
  file logFile = fopen(fileName, "w");
  string path = getPath(fileName);
  
  DebugTN("Interlocks test started. Log file: " + path); 
  
  if(ESSExtension_clearEnvironment(logFile)) {
    fprintf(logFile, "%s: *** Cancelling the test due to clearing environment failure                              *** \n", ESSExtension_formatCurrentTime()); fflush(logFile);
    fclose(logFile);    
    assert(false, "Cancelling the test due to clearing environment failure ***");
  } else {
    fprintf(logFile, "%s: *** Cleaning environment finished with success: temperatures %d, WLD %d, interlocks acknowladged *** \n", 
            ESSExtension_formatCurrentTime(), __NORMAL_TEMPERATURE, __NORMAL_WLD); fflush(logFile);
  }
  
  // test
  dyn_string failedPartitions;                           // stores every partition for which test didn't succeed 
  dyn_string faultyOverheatingSensors, faultyDssSensors, faultyWldSensors; // stores every sensor for which test didn't succeed
  //#2
  dyn_string partitions = ESSExtension_getPartitions();  
  
  for(int i = 1, limit = dynlen(partitions); i <= limit; i++) {    
    
    fprintf(logFile, "\n-------------------------------------------------------------\n"); fflush(logFile);
    fprintf(logFile, "\n%s: ### Test for partition: %s                        ### \n", ESSExtension_formatCurrentTime(), partitions[i]); fflush(logFile);

    dyn_string currentFaultyOverheatingSensors = ESSExtension_overheatingInterlockTest(                partitions[i], logFile);
    dyn_string currentFaultyWldSensors         = ESSExtension_wldInterlockTest(                        partitions[i], logFile);
    dyn_string currentDssSensors               = ESSExtension_criticalOverheatingInterlockTest(        partitions[i], logFile);
    //1#
    dynAppend(faultyOverheatingSensors, currentFaultyOverheatingSensors);
    dynAppend(faultyWldSensors,         currentFaultyWldSensors); 
    dynAppend(faultyDssSensors,         currentDssSensors);          
    //3#
    
    //#4
    if(dynlen(currentFaultyOverheatingSensors) || dynlen(currentFaultyWldSensors) || dynlen(currentDssSensors)) {
      fprintf(logFile, "%s: ### Test for partition: %s finished with FAILURE ###\n", ESSExtension_formatCurrentTime(), partitions[i]); fflush(logFile);
      dynAppend(failedPartitions, partitions[i]);
    } else {
      fprintf(logFile, "%s: ### Test for partition: %s finished with OK      ###\n", ESSExtension_formatCurrentTime(), partitions[i]); fflush(logFile);
    }  
  }
  
  //#5
  ESSExtension_performResultLog(failedPartitions, faultyOverheatingSensors, faultyWldSensors, faultyDssSensors, logFile);
  
  fprintf(logFile, "%s: *** Sensors remove     ***\n", ESSExtension_formatCurrentTime()); fflush(logFile);
  fprintf(logFile, "%s: ACTION: delete 352 temperature sensors and 8 WLD \n", ESSExtension_formatCurrentTime()); fflush(logFile);
  ESSExtension_deleteSensors();
  
  fclose(logFile);  
  
  DebugTN("Interlocks test finished. Log file: " + path);
}

/**
  Function logs the summary table:
  If test finished with failure:
  2019/07/29 12:31:47: INFO: Interlocks for EEP01, ... were completed with FAILURE
  2019/07/29 12:31:47: INFO: Following temperature sensors caused the problems during crossing overheating threshold:
  System1:TK_PLCS/ESS/crate18/module05/channel_read_01.
  System1:TK_PLCS/ESS/crate18/module05/channel_read_02.
  ...
  2019/07/29 12:31:47: INFO: Following water leakage sensors caused the problems:
  System1:TK_PLCS/ESS/crate09/module09/channel_read_01.  
  ...
  
  Otherwise:
  2019/07/30 10:42:58: INFO: Interlocks test for all partitions were successfully completed, OK
  In order to extend it add new function parameter as array and add logging loop as for different sensors here #1
  
  @param aFailedPartitions an array with failed partitions, if empty then no result table
  @param aFaultyOverheatingSensors an array with overheating sensors which finished with failure
  @param aFaultyWldSensors an array with wld sensors which finished with failure
  @param aFaultyDssSensors an array with overheating sensors which finished with failure for crossing critical overheating limit test
  @param aLogFile opened log file
*/
public void ESSExtension_performResultLog(const dyn_string &aFailedPartitions, const dyn_string &aFaultyOverheatingSensors, const dyn_string &aFaultyWldSensors, const dyn_string &aFaultyDssSensors, const file &aLogFile)
{
  // result log
  if(dynlen(aFailedPartitions)) {
    // partitions    
    fprintf(aLogFile, "%s: INFO: Interlocks for ", ESSExtension_formatCurrentTime());
    for(int i = 1, limit = dynlen(aFailedPartitions); i <= limit; i++) {
      fprintf(aLogFile, "%s ", aFailedPartitions[i]); fflush(aLogFile);
    }
    fprintf(aLogFile, "were completed with FAILURE \n", ESSExtension_formatCurrentTime());
    
    // sensors
    if(dynlen(aFaultyOverheatingSensors)) {
      fprintf(aLogFile, "%s: INFO: Following temperature sensors caused the problems during crossing overheating limit test:\n", ESSExtension_formatCurrentTime());
      for(int i = 1, limit = dynlen(aFaultyOverheatingSensors); i <= limit; i++) {
        fprintf(aLogFile, "%s\n", aFaultyOverheatingSensors[i]); fflush(aLogFile);
      }
    }
    
    if(dynlen(aFaultyWldSensors)) {
      fprintf(aLogFile, "%s: INFO: Following water leakage sensors caused the problems:\n", ESSExtension_formatCurrentTime());
      for(int i = 1, limit = dynlen(aFaultyWldSensors); i <= limit; i++) {
        fprintf(aLogFile, "%s\n", aFaultyWldSensors[i]); fflush(aLogFile);
      }    
    }
    
    if(dynlen(aFaultyDssSensors)) {
      fprintf(aLogFile, "%s: INFO: Following temperature sensors caused the problems during crossing critical overheating limit test:\n", ESSExtension_formatCurrentTime());
      for(int i = 1, limit = dynlen(aFaultyDssSensors); i <= limit; i++) {
        fprintf(aLogFile, "%s\n", aFaultyDssSensors[i]); fflush(aLogFile);
      }
    }    
    
    //#1    
  } else {
    fprintf(aLogFile, "%s: INFO: Interlocks test for all partitions were successfully completed, OK\n", ESSExtension_formatCurrentTime());
  }
}

/***********Tests***********/
/**
  Overheating interlocks test. First exceed threshold, check interlocks, return to normal value (700) and finally acknowledge interlock for the aParition.
  @param aPartition for example EEM01
  @param aLogFile opened log file
  @return array with failed overheating sensors, empty if all succeeded
*/
public dyn_string ESSExtension_overheatingInterlockTest(const string &aPartition, const file &aLogFile)
{
  int status = 0;           // variables which stores the result values for functions invoked for every sensor in the partition. If 0 then ok, otherwise 1
  dyn_string faultySensors; // array consist of sensor for which there were some errors, for example interlock does not set
  
  dyn_string sensors = ESSExtension_getSensorDps(aPartition, ESSOverheating);
  for(int i = 1, amountOfSensors = dynlen(sensors); i <= amountOfSensors; i++) {
    string overheatingSensorDp = sensors[i];
      
    if(!ESSExtension_isSensorEnabled(overheatingSensorDp)) {continue;}
      
    string essDssSensorDp = ESSExtension_getSensorDp(aPartition, i, ESSDSS);
    string fakeWriteSensor = ESSExtension_getSensorDp(aPartition, i, ESSOverheating, WRITE);
    strreplace(fakeWriteSensor, "write", "VALUEWRITE");
    
    status += ESSExtension_setValue(overheatingSensorDp, fakeWriteSensor, ESSExtension_calculateExceededRawTemperature(overheatingSensorDp, essDssSensorDp), aLogFile);
    delay(__INTERLOCK_DELAY);
    status += ESSExtension_checkInterlocks(aPartition, aLogFile, LV, HV, LVCut);  
    status += ESSExtension_setValue(overheatingSensorDp, fakeWriteSensor, __NORMAL_TEMPERATURE, aLogFile); 
    status += ESSExtension_clearInterlock(aPartition, aLogFile, LV, HV, LVCut);
    
    if(status) {
      dynAppend(faultySensors, overheatingSensorDp);
    }    
    
    status = 0; // clear status for the next iteration
    fprintf(aLogFile, "\n"); fflush(aLogFile);
  }  
 
   return faultySensors;
}

/**
  Overheating interlocks test. First exceed threshold (set value to 100 ohms), check interlocks, return to normal value (700) and finally acknowledge interlock for the aParition.
  @param aPartition for example EEM01
  @param aLogFile opened log file
  @return array with failed overheating sensors, empty if all succeeded
*/
public dyn_string ESSExtension_criticalOverheatingInterlockTest(const string &aPartition, const file &aLogFile)
{
  int status = 0;           // variables which stores the result values for functions invoked for every sensor in the partition. If 0 then ok, otherwise 1
  dyn_string faultySensors; // array consist of sensor for which there were some errors, for example interlock does not set
  
  dyn_string sensors = ESSExtension_getSensorDps(aPartition, ESSOverheating);
  for(int i = 1, amountOfSensors = dynlen(sensors); i <= amountOfSensors; i++) {
    string overheatingSensorDp = sensors[i];
      
    if(!ESSExtension_isSensorEnabled(overheatingSensorDp)) {continue;}
      
    string essDssSensorDp = ESSExtension_getSensorDp(aPartition, i, ESSDSS);
    string fakeWriteSensor = ESSExtension_getSensorDp(aPartition, i, ESSOverheating, WRITE);
    strreplace(fakeWriteSensor, "write", "VALUEWRITE");
    
    status += ESSExtension_setValue(overheatingSensorDp, fakeWriteSensor, 100, aLogFile);
    delay(__INTERLOCK_DELAY);
    
    status += ESSExtension_checkInterlocks(aPartition, aLogFile, LV, HV, LVCut);  
    
    if(ESSExtension_isGeneralInterlockSet(DSS_CRITICAL_Overheating)) {
      fprintf(aLogFile, "%s:\tEVENT: General interlock %s set OK\n", ESSExtension_formatCurrentTime(), DSS_CRITICAL_Overheating); fflush(aLogFile);
    } else {
      fprintf(aLogFile, "%s:\tEVENT: General interlock %s not set when expected, FAILURE\n", ESSExtension_formatCurrentTime(), DSS_CRITICAL_Overheating); fflush(aLogFile);
      status += 1;
    }    
    
    status += ESSExtension_setValue(overheatingSensorDp, fakeWriteSensor, __NORMAL_TEMPERATURE, aLogFile); 
    status += ESSExtension_clearInterlock(aPartition, aLogFile, LV, HV, LVCut);
    
    if(!ESSExtension_isGeneralInterlockSet(DSS_CRITICAL_Overheating)) {
      fprintf(aLogFile, "%s:\tEVENT: General interlock acknowledged OK\n", ESSExtension_formatCurrentTime(), DSS_CRITICAL_Overheating); fflush(aLogFile);
    } else {
      fprintf(aLogFile, "%s:\tEVENT: General interlock still fired, FAILURE\n", ESSExtension_formatCurrentTime(), DSS_CRITICAL_Overheating); fflush(aLogFile);
      status += 1;
    }
    
    if(status) {
      dynAppend(faultySensors, overheatingSensorDp);
    }    
    
    status = 0; // clear status for the next iteration
    fprintf(aLogFile, "\n"); fflush(aLogFile);
  }  
 
   return faultySensors;
}

/**
  Wld interlocks test. First exceed threshold, check interlocks, return to normal value (4500) and finaly acknowledge interlock for the aParition.
  @param aPartition for example EEM01
  @param aLogFile opened log file
  @return array with failed wld sensors, empty if all succeeded
*/
public dyn_string ESSExtension_wldInterlockTest(const string &aPartition, const file &aLogFile)
{
  int status = 0;           // variables which stores the result values for functions invoked for every sensor in the partition. If 0 then ok, otherwise 1
  dyn_string faultySensors; // array consist of sensor for which there were some errors for example interlock does not set
  
  if(ESSExtension_isBarrel(ESSExtension_getPosition(aPartition))){ // only for endcaps
    return faultySensors; // empty
  }
  
  string wld = ESSExtension_getWaterLeakageSensor(aPartition);
      
  if(!ESSExtension_isSensorEnabled(wld)) {return faultySensors;}
      
  string fakeWriteSensor = wld;
  strreplace(fakeWriteSensor, "read", "VALUEWRITE");
    
  status += ESSExtension_setValue(wld, fakeWriteSensor, __EXCEEDED_WLD, aLogFile);
  delay(__INTERLOCK_DELAY_WLD);
  status += ESSExtension_checkInterlocks(aPartition, aLogFile, COOL);  
  status += ESSExtension_setValue(wld, fakeWriteSensor, __NORMAL_WLD, aLogFile); 
  status += ESSExtension_clearInterlock(aPartition, aLogFile, COOL);
    
  if(status) {
    dynAppend(faultySensors, wld);
  }   
  
  fprintf(aLogFile, "\n"); fflush(aLogFile);
 
  return faultySensors;
}

/*******************library***********************/
/**
  This function checks if interlocks were properly set for aPartition and not set for any other. If fired for any other, then test is finished.
  aFirst, aSecond, aThird, aFourth - interlocks with any combination, but have to be LV, HV, LVCut or COOL. Function can be invoked for one/two/three or four interlocks
  If for example COOL passed as a aFirst and nothing for others then only COOL will be checked, that is if was indeed set for aPartition and not set for any other
  @param aPartition for example EEM01
  @param aLogFile opened log file
  @param aFirst LV, HV, LVCut or COOL, default LV
  @param aSecond LV, HV, LVCut or COOL, default ""
  @param aThird LV, HV, LVCut or COOL, default ""
  @param aFourth LV, HV, LVCut or COOL, default ""

  @return 0 if succeed, otherwise 1
 */
public int ESSExtension_checkInterlocks(const string &aPartition, const file &aLogFile, const string aFirst = LV, const string aSecond = "", const string aThird = "", const string aFourth = "")
{
  int status = 0; // if interlock was set then 0, otherwise 1  
  
  // check if interlocks fired for aPartition
  bool isFirstSet  = aFirst  != "" ? ESSExtension_isInterlockSet(aPartition, aFirst) : true;
  bool isSecondSet = aSecond != "" ? ESSExtension_isInterlockSet(aPartition, aSecond): true;
  bool isThirdSet  = aThird  != "" ? ESSExtension_isInterlockSet(aPartition, aThird) : true;
  bool isForthSet  = aFourth  != "" ? ESSExtension_isInterlockSet(aPartition, aFourth) : true;
  
  const string INTERLOCK_MESSAGE = aFirst + " " + aSecond + " " + aThird + " " + aFourth;  // to printf function
  
  if(isFirstSet && isSecondSet && isThirdSet && isForthSet) {
    fprintf(aLogFile, "%s:\tEVENT: Interlock %s set for the partition %s, OK\n",                         ESSExtension_formatCurrentTime(), INTERLOCK_MESSAGE, aPartition); fflush(aLogFile);
  } else {
    fprintf(aLogFile, "%s:\tEVENT: Interlock %s not set for the partition %s, when expected, FAILURE\n", ESSExtension_formatCurrentTime(), INTERLOCK_MESSAGE, aPartition); fflush(aLogFile);
    status = 1;
  }
  
  dyn_string firedPartitions;
  dyn_string partitions = ESSExtension_getPartitions();
  
  // check other partitions if interlocks were fired for them (not expected)
  for(int i = 1, limit = dynlen(partitions); i <= limit; i++) {
  
    //  if first quadrant: without it script signals error, because when EEP01 is fired EEP02 in ESSExtension_isInterlockSet is resolved to EEP01.
    //  In the result second condition below shows that interlock was fired for not proper partition - EEP02
    bool isSecondQuadrant = ESSExtension_resolvePartitionName(partitions[i], READ, aInterlockType) != ESSExtension_resolvePartitionName(aPartition, READ, aInterlockType);

    bool isOtherFirstSet  = aFirst  != "" ? ESSExtension_isInterlockSet(partitions[i], aFirst) : false;
    bool isOtherSecondSet = aSecond != "" ? ESSExtension_isInterlockSet(partitions[i], aSecond): false;
    bool isOtherThirdSet  = aThird  != "" ? ESSExtension_isInterlockSet(partitions[i], aThird) : false;
    bool isOtherForthSet  = aFourth  != "" ? ESSExtension_isInterlockSet(partitions[i], aFourth) : false;    

    bool isAnyOtherSet    = isOtherFirstSet || isOtherSecondSet || isOtherThirdSet || isOtherForthSet;
            
    if(isAnyOtherSet && isSecondQuadrant) { 
      dynAppend(firedPartitions, partitions[i]);
    }
  }
  
  if(ESSExtension_logFiredPartitionsInformation(firedPartitions, aLogFile)) {
    fclose(aLogFile);        
    assert(false, "Wrong interlock set"); // environment not clean
  }
  
  return status;
}

/**
  This function clears LV, HV, LVCut or COOL interlocks for aPartition. If number of attemps exceeded __MAX_ACKNOWLADGE_ATTEMPS, then finish script. 
  If cleared, but not before __EXPECTED_ACKNOWLADGE_ATTEMPTS, then returns 1, otherwise 0.
  aFirst, aSecond, aThird, aFourth - interlocks with any combination, but have to be LV, HV, LVCut or COOL.
  Function can be invoked for one/two/three or four interlocks, then only one/two/three or four interlocks will be cleared
  
  @param aPartition for example EEM01
  @param aLogFile opened log file
  @param aFirst LV, HV, LVCut or COOL, default LV
  @param aSecond LV, HV, LVCut or COOL, default ""
  @param aThird LV, HV, LVCut or COOL, default ""
  @param aFourth LV, HV, LVCut or COOL, default ""

  @return 0 if succeed, otherwise 1
*/
public int ESSExtension_clearInterlock(const string &aPartition, const file &aLogFile, const string aFirst = LV, const string aSecond = "", const string aThird = "", const string aFourth = "")
{
  int amountOfAttempts = 0;  // amount of attemps to acknowledge the interlock
  
  const string INTERLOCK_MESSAGE = aFirst + " " + aSecond + " " + aThird + " " + aFourth;  
  
  while(true) {
     
    bool isFirstSet  = aFirst  != "" ? ESSExtension_isInterlockSet(aPartition, aFirst) : false; // true
    bool isSecondSet = aSecond != "" ? ESSExtension_isInterlockSet(aPartition, aSecond): false;
    bool isThirdSet  = aThird  != "" ? ESSExtension_isInterlockSet(aPartition, aThird) : false;
    bool isForthSet  = aFourth  != "" ? ESSExtension_isInterlockSet(aPartition, aFourth) : false;  

    // if still fired, then acknowladge
    if(amountOfAttempts <= __MAX_ACKNOWLADGE_ATTEMPS && (isFirstSet || isSecondSet || isThirdSet || isForthSet)) {
      ESSExtension_acknowledgePartition(aPartition);
      amountOfAttempts++;
      delay(__ACKNOWLADGE_DELAY);  
    } else {
      break;    
    }
  }    
  
  if(amountOfAttempts > __MAX_ACKNOWLADGE_ATTEMPS) {
    fprintf(aLogFile, "%s: ACTION:   Acknowladging interlock " + INTERLOCK_MESSAGE + " %s: FAILURE exceeded max amount of %d attemps FAILURE\n", 
            ESSExtension_formatCurrentTime(), aPartition, amountOfAttempts); fflush(aLogFile);
    fclose(logFile);    
    assert(false, "Acknowladging interlock FAILURE"); // environment not clean - finish
  }
  
  if(__EXPECTED_ACKNOWLADGE_ATTEMPTS < amountOfAttempts) {
    fprintf(aLogFile, "%s: ACTION:   Acknowladging interlock " + INTERLOCK_MESSAGE + " for %s: success after %d attemps when expected %d FAILURE\n", 
            ESSExtension_formatCurrentTime(), aPartition, amountOfAttempts, __EXPECTED_ACKNOWLADGE_ATTEMPTS); fflush(aLogFile);
    return 1;
  }
  
  if(__EXPECTED_ACKNOWLADGE_ATTEMPTS == amountOfAttempts) {
    fprintf(aLogFile, "%s: ACTION:   Acknowladging interlock " + INTERLOCK_MESSAGE + " for %s: success after %d attemps OK\n",
           ESSExtension_formatCurrentTime(), aPartition, amountOfAttempts); fflush(aLogFile);
    return 0;
  }
  
  return 0;
}

/**
  This function sets aNewValue (resistance) for aFakeWriteSensor and checks if value were properly rewriten into aSensorDp (in PLC). If yes then 0, otherwise 1.
  Invoke cause 5 seconds delay.  
  
  @param aSensorDp real sensor
  @param aFakeWriteSensor fake sensor
  @param aNewValue new value to set
  @param aLogFile opened log file
  
  @return 0 if succeed, otherwise 1
*/
public int ESSExtension_setValue(const string &aSensorDp, const string &aFakeWriteSensor, const int &aNewValue, const file &aLogFile)
{
  int valueWas;  // previous temperature value
  dpGet(aSensorDp + "value.raw", valueWas);  
  
  dpSet(aFakeWriteSensor + "value", aNewValue); 
    
  delay(__REWRITE_DELAY); //  for plc        

  // check if value is rewritten
  int valueIs;
  dpGet(aSensorDp + "value.raw", valueIs);
    
  if(valueIs != aNewValue) {
    fprintf(aLogFile, "%s: ACTION:   Setting %d value for sensor %s: %d -> %d FAILURE\n", ESSExtension_formatCurrentTime(), aNewValue, aSensorDp, valueWas, valueIs); fflush(aLogFile);
    return 1;
  } else {
    fprintf(aLogFile, "%s: ACTION:   Setting %d value for sensor %s: %d -> %d OK\n", ESSExtension_formatCurrentTime(), aNewValue, aSensorDp, valueWas, valueIs); fflush(aLogFile);
    return 0;
  }
}

/**
  Calculates aritmetic average of temperatures from ESSOverheating and ESSDSS sensors. Value will be between critical overheating and normal overheating
  
  @param aOverheatingSensorDp Overheating sensor with lower limit
  @param aEssDssSensorDp DSS sensor with higher limit
  
  @return 0 if succeed, otherwise 1
*/
public int ESSExtension_calculateExceededRawTemperature(const string &aOverheatingSensorDp,const string &aEssDssSensorDp)
{
  int temperatureLowerLimit;
  dpGet(aOverheatingSensorDp + "lowerLimit.raw", temperatureLowerLimit);
  
  int temperatureHigherLimit;
  dpGet(aEssDssSensorDp + "lowerLimit.raw", temperatureHigherLimit);  
  
  return temperatureHigherLimit + (temperatureLowerLimit - temperatureHigherLimit) / 2;
}

public string ESSExtension_formatCurrentTime()
{
  return formatTime("%Y/%m/%d %H:%M:%S", getCurrentTime());
}

/**
  Checks if interlock is set basing on normalValue and value. If it is not equal, then not set otherwise set.
  @param aPartition for example EEM01 
  @param aInterlockType LV, LVCut or COOL
  @return true if interlock is set, otherwise false
*/
public bool ESSExtension_isInterlockSet(const string &aPartition, const string &aInterlockType)
{     
  string interlockDp = ESSExtension_getRelay(ESSExtension_resolvePartitionName(aPartition), READ, aInterlockType); // resolve because
                                                                                                                   // for EEP02, EEP04, EEM02, EEM04 there is no relay so EEM01 etc. 
  bool currentValue, normalValue;
  
  dpGet(interlockDp + "value", currentValue);  
  dpGet(interlockDp + "normalValue", normalValue);
    
  return (normalValue != currentValue);
}

/**
  Checks if interlock is set basing on normalValue and value. If it is not equal, then not set otherwise set.
  @param aInterlockType ESS_PLC_Watchdog, COOL_PLC_Watchdog, ESS_PLC_Alive, COOL_PLC_Alive, DSS_CRITICAL_Overheating
  @return true if interlock is set, otherwise false
*/
public bool ESSExtension_isGeneralInterlockSet(const string &aInterlockType)
{     
  string interlockDp = ESSExtension_getGeneralRelay(READ, aInterlockType);

  bool currentValue, normalValue;
  
  dpGet(interlockDp + "value", currentValue);  
  dpGet(interlockDp + "normalValue", normalValue);
    
  return (normalValue != currentValue);
}

/**
  Function logs the partitions which are not expected fired and logs information about them.
  0 if no partition was fired, 1 if one or more were fired
  
  @param aFiredPartitions an array with failed partitions, if empty then no result table
  @param aLogFile opened log file
  @return 0 if aFiredPartitions is empty, 1 otherwise
*/
public int ESSExtension_logFiredPartitionsInformation(const dyn_string &aFiredPartitions, const file &aLogFile) 
{
  if(dynlen(aFiredPartitions)) {
    fprintf(aLogFile, "%s: INFO: Interlocks for ", ESSExtension_formatCurrentTime()); fflush(aLogFile);
    
    for(int i = 1, limit = dynlen(aFiredPartitions); i <= limit; i++) {
      fprintf(aLogFile, "%s ", aFiredPartitions[i]); fflush(aLogFile);
    }
     
    fprintf(aLogFile, "fired when not expected FAILURE\n", ESSExtension_formatCurrentTime()); fflush(aLogFile);
    
    return 1;
  }
  
  return 0;
}

/**************clear environment functions**************/
/**
  This function sets all termistors values for 700, wld for 4500, and acknowledge interlocks for every partrition. 
  If cleaning is performed without problems, then 0 is returned otherwise 1.
  
  @param aLogFile opened log file
  @return 0 if environment is cleaned properly, 1 otherwise
 */
public int ESSExtension_clearEnvironment(const file &aLogFile)
{  
  fprintf(aLogFile, "%s: *** Sensors creation     ***\n", ESSExtension_formatCurrentTime()); fflush(aLogFile);
  fprintf(aLogFile, "%s: ACTION: creation of 352 temperature sensors and 8 WLD \n", ESSExtension_formatCurrentTime()); fflush(aLogFile);  
  
  ESSExtension_createSensors();  
  
  fprintf(aLogFile, "%s: *** Cleaning environment ***\n", ESSExtension_formatCurrentTime()); fflush(aLogFile);
  fprintf(aLogFile, "%s: ACTION: temperatures to %d, WLD to %d\n", ESSExtension_formatCurrentTime(), __NORMAL_TEMPERATURE, __NORMAL_WLD); fflush(aLogFile);    
  
  // overheating 700, wld 4500 and acknowladge interlocks
  ESSExtension_setNormalState(aLogFile);
    
  delay(5); // wait for plc
  
  // check if sensor have normal values
  if(ESSExtension_checkNormalState()) {return 1;}

  // check if interlocks cleaned and log information if not
  if(ESSExtension_checkInterlocksForClearEnvironment(aLogFile)) {return 1;}
  
  return 0;
}

/**
  Checks if any of the partition is fired and log about them information. 1 if any was fired, 0 otherwise
  @param aLogFile opened log file
  @return 0 if no interlock is set, 1 otherwise
*/
public int ESSExtension_checkInterlocksForClearEnvironment(const file &aLogFile)
{
  dyn_string firedPartitions;
  dyn_string partitions = ESSExtension_getPartitions();  
  for(int i = 1, partitionsAmount = dynlen(partitions); i <= partitionsAmount; i++) {
    bool isAnyInterlocksSet = ESSExtension_isInterlockSet(partitions[i], LV) || ESSExtension_isInterlockSet(partitions[i], HV) 
                              || ESSExtension_isInterlockSet(partitions[i], LVCut) || ESSExtension_isInterlockSet(partitions[i], COOL);
    if(isAnyInterlocksSet) { 
      dynAppend(firedPartitions, partitions[i]);
    }
  }
  
  return ESSExtension_logFiredPartitionsInformation(firedPartitions, aLogFile) ;
}

/**
  Function sets 700 to overheating sensors, 4500 to WLD and acknowladge interlocks
  @param aLogFile opened log file
*/
public void ESSExtension_setNormalState(const file &aLogFile)
{
  // temperature
  dyn_string fakeWriteSensors, aliasesOver;
  dpGetAllAliases(fakeWriteSensors, aliasesOver, "ESS/Overheating/*/VALUEWRITE/*");  
  for(int i = 1, sensorsAmount = dynlen(fakeWriteSensors); i <= sensorsAmount; i++) { 
    dpSet(fakeWriteSensors[i] + "value", __NORMAL_TEMPERATURE);
    delay(0, 80);
  }
  
  dyn_string fakeWld, aliasesWld;
  dpGetAllAliases(fakeWld, aliasesWld, "ESS/Waterleak/*/VALUEWRITE/*");  
  for(int i = 1, sensorsAmount = dynlen(fakeWld); i <= sensorsAmount; i++) { 
    dpSet(fakeWld[i] + "value", __NORMAL_WLD);
    delay(0, 80);
  }
  
  fprintf(aLogFile, "%s: ACTION: acknowladging interlocks ***\n", ESSExtension_formatCurrentTime()); fflush(aLogFile);
  
  dyn_string partitions = ESSExtension_getPartitions();  
  for(int i = 1, partitionsAmount = dynlen(partitions); i <= partitionsAmount; i++) {
    ESSExtension_acknowledgePartition(partitions[i]);
  }
}

/**
  Function checks if sensors are in normal state, that is overheating == 700, WLD == 4500. 0 if ok otherwise 1
  
  @return 0 if normal state, 1 otherwise
*/
public int ESSExtension_checkNormalState()
{
  dyn_string readSensors, readAliases;
  dpGetAllAliases(readSensors, readAliases, "ESS/Overheating/*/Read/*");
  for(int i = 1, sensorsAmount = dynlen(readSensors); i <= sensorsAmount; i++) { 
    int value;
    dpGet(readSensors[i] + "value.raw", value);
    if(value != __NORMAL_TEMPERATURE) { return 1;} 
  }
  
  dyn_string readSensorsWld, readAliasesWld;
  dpGetAllAliases(readSensorsWld, readAliasesWld, "ESS/Waterleak/*/Read/*");
  for(int i = 1, sensorsAmount = dynlen(readSensorsWld); i <= sensorsAmount; i++) { 
    int value;
    dpGet(readSensorsWld[i] + "value.raw", value);  
    if(value != __NORMAL_WLD) { return 1;}
  }   
  
  return 0;
}

/********************Delete and create fake sensors*********************/

/**
  Function creates set of fake overheating sensors from DB800.DBW0 to DB800.DBW1406 
  and wld from DB800.DBW1408 to DB800.DBW1422
*/
public void ESSExtension_createSensors() 
{
  __ESSExtension_createTemperature();
  
  __ESSExtension_createWld();
  
  dyn_string dps = dpNames("*", "FwAi");
  DebugTN(dynlen(dps));
}

/**
  Delete every fake sensor
*/
public void ESSExtension_deleteSensors()
{
  dyn_string dps, aliases;
  dpGetAllAliases(dps, aliases, "*VALUEWRITE*");
  
  for(int i = 1, limit = dynlen(dps); i <= limit; i++) {
    dpDelete(dps[i]);
  }
}

private void __ESSExtension_createTemperature()
{
  dyn_string sensorDps, aliases;
  dpGetAllAliases(sensorDps, aliases, "ESS/Overheating/*/Read/Sensor*");
  
  for(int i = 1, amount = dynlen(sensorDps); i <= amount; i++) {
    string newName = dpSubStr(sensorDps[i], DPSUB_DP);
    strreplace(newName, "read", "VALUEWRITE");
    dpCreate(newName, "FwAi");

    //alias
    string newAlias = dpGetAlias(sensorDps[i]);
    strreplace(newAlias, "Read", "VALUEWRITE");
    dpSetAlias(newName + ".", newAlias);
    
    int piw = -1;
    dpGet(sensorDps[i]+"piw.readBack", piw);

    __CMS_ECALfw_ESS_createESSAddress(newName + ".value", 800, (piw - 288)); // DB800.DBW0 - DB800.DBW1406
  }
}

private void __ESSExtension_createWld()
{
  dyn_string sensorDps, aliases;
  dpGetAllAliases(sensorDps, aliases, "ESS/Waterleak/*/Read/Sensor*");

  for(int i = 1, amount = dynlen(sensorDps); i <= amount; i++) {
    string newName = dpSubStr(sensorDps[i], DPSUB_DP);
    strreplace(newName, "read", "VALUEWRITE");
    dpCreate(newName, "FwAi");

    //alias
    string newAlias = dpGetAlias(sensorDps[i]);
    strreplace(newAlias, "Read", "VALUEWRITE");
    dpSetAlias(newName + ".", newAlias);

    __CMS_ECALfw_ESS_createESSAddress(newName + ".value", 800, 1408 + (i-1)*2); // DB800.DBW1408 - DB800.DBW1422
  }
}

/**
* Create S7 periphery address
* Step7 transformation types - See PVSS help for _address
    UNDEFINED 700 Undefined
    INT16     701 16-Bit Integer signed
    INT32     702 32-Bit Integer signed
    UINT16    703 16-Bit Integer unsigned
*/
private void __CMS_ECALfw_ESS_createESSAddress(string dpe, int DBNumber, int DBWord) {
  //! [Example: create S7 periphery address]
  dyn_anytype params;
  dyn_string exc;
  params[fwPeriphAddress_TYPE]          = fwPeriphAddress_TYPE_S7;
  params[fwPeriphAddress_DRIVER_NUMBER] = 2;
  params[fwPeriphAddress_ROOT_NAME]     = "PLC_ESS.DB" + DBNumber + ".DBW" + DBWord; //address
  params[fwPeriphAddress_DIRECTION]     = DPATTR_ADDR_MODE_IO_POLL; //DPATTR_ADDR_MODE_INPUT_POLL; //read mode
  params[fwPeriphAddress_DATATYPE]      = 703; //default type convertion. see PVSS help on _address for more options
  params[fwPeriphAddress_ACTIVE]        = TRUE;
  params[fwPeriphAddress_S7_LOWLEVEL]   = FALSE; //or true if you want timestamp to be updated only on value change
  params[FW_PARAMETER_FIELD_INTERVAL]   = 3;
  params[fwPeriphAddress_S7_POLL_GROUP] = "_fastpoll"; //poll group name
  
  fwPeriphAddress_set(dpe, params,  exc);
}
