#uses "CMS_ECALfw_ESSExtension/ESSExtension_sensorsManipulation.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

void main()
{
  string dataDirectory = "data/CMS_ECALfw_ESSExtension/data/";
  if(!isdir(dataDirectory)) {
    mkdir(dataDirectory, "777");  
  }
  
  string fileName = dataDirectory + "conversion_test_" + formatTime("%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".log";
  file logFile = fopen(fileName, "w");
  string path = getPath(fileName);
  
  DebugTN("Conversion test started. Log file: " + path);  
  
  dyn_string aliases;
  dyn_string writeSensors, writeSensorsDss;
  
  dpGetAllAliases(writeSensors, aliases, "ESS/Overheating/*/Write/Sensor*");
  dpGetAllAliases(writeSensorsDss, aliases, "ESS/DSS/*/Write/Sensor*");
  dynAppend(writeSensors, writeSensorsDss);
  
  fprintf(logFile, "Test for %d sensors: Overheating and DSS\n\n", dynlen(writeSensors));  
  
  for(int i = 1, limit = dynlen(writeSensors); i <= limit; i++) {
    test_checkDpConversionFunctionForSensor(writeSensors[i], logFile);
  }
  
  DebugTN("Conversion test finished. Log file: " + path);  

  fclose(logFile);
}

/**
  Test rewrites temperature value for the sensor from read.value.converted to write.value.converted and checks if calculated by dpFunc value in write.value (raw) is the same as read.value (raw)
*/
void test_checkDpConversionFunctionForSensor(const string &aWriteSensor, const file &aLogFile)
{
  string readSensor = aWriteSensor;
  strreplace(readSensor, "write", "read");
    
  int readLimitRaw, writeLimitRaw;
  float readLimitConverted;
    
  dpGet(readSensor + "lowerLimit.raw", readLimitRaw);
  dpGet(readSensor + "lowerLimit.converted", readLimitConverted);
    
  dpSetWait(aWriteSensor + "lowerLimit.converted", readLimitConverted);
  dpGet(aWriteSensor + "lowerLimit.raw", writeLimitRaw);
  
  fprintf(aLogFile, "--- %s and %s ---\n", readSensor, aWriteSensor);
  fprintf(aLogFile, "Read  raw to converted: %9d ->dpFunct-> %9.4f\n", readLimitRaw, readLimitConverted);
  string status = readLimitRaw == writeLimitRaw ? "OK" : "FAILURE";
  fprintf(aLogFile, "Write converted to raw: %9.4f ->dpFunct-> %9d %s\n\n", readLimitConverted, writeLimitRaw, status);
}
