/*! @page ECAL Safety System - PLC Interlocks display
 
    @tableofcontents 
  

    @section brief Brief 
    This article describes how to use <b>CMS_ECALfw_ESSExtension.pnl</b> panel, which gives overall look at the interlocks in the 44 ECAL partitions as well as temperature sensors.. 
    @image html CMS_ECALfw_ESSExtension_1.png

    @section Description
    Panel is divided into 44 sections, where each represents an <b>ECAL</b> partition. 36 these partitions are grouped into <b>barrel</b> partitions on the left part of the panel,
    whereas on the right side endcaps partitions are presented. 

    Each partition consist of 8 temperature sensors. Sensors are grouped into 2 rows. On the upper row even sensors are placed, on the lower odd ones. 
    In order to check a detailed description about the particular sensor, tooltip will appear when the mouse is moved into the sensor. 

    4 green rectangles in every parititon represent interlocks. From the left they are: cooling interlock (<b>COOL</b>), high voltage (<b>HV</b>), low voltage cut (<b>LVCut</b>), and finally low voltage (<b>LV</b>).
    Similarly to the sensors, when the mouse is moved into the particular interlock the tooltip will appear.

    In the endcap section only even partitions have interlocks. They are two endcaps, each of them consisits of 2 elements called D,
    Each D constists of two partition, but interlocks are per D not per partition in the endcap section, so interlocks in the odd endcap partitions are not displayed. 

    @image html CMS_ECALfw_ESSExtension_main_description.png
    
    @section Logic
    Red interlocks rectangles signalise that, some sensors detected crossing water leakage limit or overheating threshold for them. 
   
    @subsection temp Overheating
    When temperature increases to much and cross the sensors limit,
    then HV, LVCut and LV interlock will be automatically set. Sensor which caused interlock can be found within the group of 8 sensors for the partition where that interlock was set. 
    Worth to note is the fact that when sensor is grayed out, so the sensor is disabled it does not cause setting automatic interlock. 
    
    @subsection water Water leakage
    When COOL interlock is set it means that value in water leakage sensor crossed specific for that sensor limit. Water leakage sensors are not displayed in that panel.

    Fired interlocks can be seen on the picture below.
    @image html CMS_ECALfw_ESSExtension_overheating.png

    @section operating Operating the panel

    @subsection clearing Clearing interlocks
    When the temperature sensors come back to the normal state, so below the overheating threshold, it is possible to clean the interlock, that is acknowledge alarms which were caused, by 
    the overheating state. In order to do that, click on the checkbox in the right upper corner in the partition where you would like to clear the interlocks and click on <b>Clear Interlocks</b>
    button on the top of the panel. 
    @image html CMS_ECALfw_ESSExtension_clearing.png
    
    Optionally it is possible to clear the interlocks per ECAL section: <b>EE+</b>, <b>EE-</b>, <b>EB+</b>, <b>EB-</b>. To achieve this select specific partition section on the left corner of the panel
    and click on the button "Clear Interlocks"
    @image html CMS_ECALfw_ESSExtension_clearing_2.png

    @section manual Manual interlock
    In order to set manual interlock check a partition for which you would like to set an interlock in the similar way as if you would like to clear interlock, what was described in the subsection 
    Clearing interlocks. After that click on the <b>Set Manual Interlock</b> button. Interlocks for checked partitions will be set, what is indicated by <b>M</b> letter on the interlock rectangle.
    Action sets HV, LVCut and HV interlock. It is not possible to set COOL interlock.
   
    @image html CMS_ECALfw_ESSExtension_manual.png
*/
